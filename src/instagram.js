const puppeteer = require ('puppeteer');
const BASE_URL = 'https://www.instagram.com/';
const TAG_URL = (tag) => `https://www.instagram.com/explore/tags/${tag}/`; //results by tag page

const target = {
    browser: null,
    page: null,

    initialize: async () => {
        target.browser = await puppeteer.launch({
            headless: false
        });

        target.page = await target.browser.newPage();

    },

    loginLogic: async (USER_NAME, USER_PASS) => {

        // Log in with account credentials
        await target.page.goto(BASE_URL);
        await target.page.waitFor(1000);

        await target.page.type('input[name="username"]', USER_NAME, {delay: 100});
        await target.page.type('input[name="password"]', USER_PASS, {delay: 100});

        let loginButton = await target.page.$('#react-root > section > main > article > div.rgFsT > div:nth-child(1) > div > form > div:nth-child(4)');
        await loginButton.click();

        await target.page.waitFor(3000);
    },

    likeLogic: async (tags = []) => {
        for (let tag of tags) {
            await target.page.goto(TAG_URL(tag));
            await target.page.waitFor(5000);

            let posts = await target.page.$$('article > div:nth-child(3) img[decoding="auto"]'); //$$ means select all

            //get only first 10
            for(let i = 0; i < 20; i++) {
                let post = posts[i];

                //click on post one by one
                await post.click();
                await target.page.waitFor(2000);
                //set like
                const button = await target.page.$('body > div._2dDPU.CkGkG > div.zZYga > div > article > div.eo2As > section.ltpMr.Slqrh > span.fr66n > button');
                if (button) await button.click();
                await target.page.waitFor(17000);

                //close the liked post
                let closeButton = await target.page.$('body > div._2dDPU.CkGkG > div.Igw0E.IwRSH.eGOV_._4EzTm.BI4qX.qJPeX.fm1AK.TxciK.yiMZG > button')
                await closeButton.click();
                await target.page.waitFor(5000);
            }
            await target.page.waitFor(2000);
        }
    },

    endSession: async () => {

        target.browser = await target.browser.close();
    }
}

module.exports = target;